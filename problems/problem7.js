//    Write a function that accesses and prints the names and email addresses of individuals aged 25.

function printNamesAndEmailsOf25YearOlds(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    }
    for(let person of personList) {
        if(person.age == 25) {
            console.log(`Name: ${person.name}, email: ${person.email}`);
        }
    }
} 

module.exports.printNamesAndEmailsOf25YearOlds = printNamesAndEmailsOf25YearOlds;
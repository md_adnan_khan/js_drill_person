//    Create a function to retrieve and display the first hobby of each individual in the dataset.

function displayFirstHobby(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    }
    for(let person of personList) {
        console.log(`${person.hobbies[0]}`);
    }
}

module.exports.displayFirstHobby = displayFirstHobby;
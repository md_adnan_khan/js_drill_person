//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function displayStudentNameAndCity(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    }
    console.log(` person: { name: ${personList[3].name},  city: ${personList[3].city} }`);
}

module.exports.displayStudentNameAndCity = displayStudentNameAndCity;

function getEmailsFrompersonList(personList) {  
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
    } 

    let individualEmailList = []; 
    for(let individual of personList) {
        individualEmailList.push(individual.email); // push the emails into the individualEmailList array
    }
    
    return individualEmailList;
}

module.exports.getEmailsFrompersonList = getEmailsFrompersonList;
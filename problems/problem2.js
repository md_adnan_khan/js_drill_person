//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

function displayIndividualHobby(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    } 

    for(let person of personList) {
        console.log(person.hobbies);
    }
}

module.exports.displayIndividualHobby = displayIndividualHobby;
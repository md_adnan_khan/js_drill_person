//    Implement a loop to access and log the city and country of each individual in the dataset.

function printCityAndCountryOfIndividual(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    }
    for(let person of personList) {
        console.log(`country: ${person.country}, city: ${person.city}`);
    }
}

module.exports.printCityAndCountryOfIndividual = printCityAndCountryOfIndividual;
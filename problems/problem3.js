//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function displayStudentNamesInAustralia(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    } 

    for(let person of personList) {
        if(person.isStudent == true && person.country == "Australia") {
            console.log(`${person.name} `);
        }
    }
}

module.exports.displayStudentNamesInAustralia = displayStudentNamesInAustralia;
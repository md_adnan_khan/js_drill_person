//    Implement a loop to access and print the ages of all individuals in the dataset.

function printAgesOfPersons(personList) {
    if(personList === null || personList === undefined || personList.length === 0 || !Array.isArray(personList)) { //handle error 
        console.log(`Input is not a valid array, Enter a valid array`);
        return;
    }
    
    for(let person of personList) {
        console.log(`${person.age}`);
    }
}

module.exports.printAgesOfPersons = printAgesOfPersons;
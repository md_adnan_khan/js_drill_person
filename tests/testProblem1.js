let {getEmailsFrompersonList} = require('../problems/problem1.js');
let {arrayOfObjects} = require('../personData/personsData.js');

let individualEmailList = getEmailsFrompersonList(arrayOfObjects);

if(individualEmailList !== undefined) {
    console.log(individualEmailList); // print on the window
}